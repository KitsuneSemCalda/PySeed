#!/bin/bash

install(){
    poetry install
    chmod +x src/main.py 
    cp -r src/ /usr/local/bin/PySeed
    ln -sf /usr/local/bin/PySeed/main.py /usr/bin/PySeed
}

if [ "$(id -u)" -eq 0 ]; then
    install
else
    echo "Você precisa de privilégios de root para executar este script."
fi
