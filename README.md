# PySeed

Um requester que envia dados falsos para uma api rest.

## How to install

Para instalar o PySeed, automatizamos o processo com o ShellScript, sendo necessário rodar apenas `sudo bash install.sh` para instalar este script em sistemas unix-like.<br>
<br>
O instalador segue os seguintes passos:<br>
  1. Instalar os requirementos com `poetry install`<br>
  2. Alterar permissão para execução do script com `chmod +x src/main.py`<br>
  3. Mover o arquivo src/main.py para a pasta /usr/local/bin/pyseed com `mv src/main.py /usr/local/bin/pyseed`<br>

## How works

O pyseed intancia a classe Faker da biblioteca faker instalada, com isso a cada interação do loop selecionado atráves da flag --counter podemos gerar e requisitar o envio de dados falsos para o usuário atráves do endereço `http://127.0.0.1:3333/register` podendo o endereço ser modificado pelas flags --url para modificar a url após "http://", a flag --port para modificar a porta de onde saí o método post após os ":" e a flag --route que altera a rota por onde a  requisição irá passar.<br>

**Ex:**<br>

`pyseed --counter 30` -> Este comando faz com que o pyseed gere trinta requisições e envie informações de username, email e password pela url `http://127.0.0.1:3333/register`.<br>

`pyseed --url 192.168.1.1` -> Este comando faz com que o pyseed gere quinze requisições e envie informações de username, email e password pela url `http://192.168.1.1:3333/register`.<br>

`pyseed --port 5432` -> Este comando faz com que o pyseed gere quinze requisições e informações de username, email e password pela port `http://127.0.0.1:5432/register`.<br>

`pyseed --route register/user` -> Este comando faz com que o pyseed gere quinze requisições e envie informações de username, email e password pela url `http://127.0.0.1:3333/register/user`.<br>

Cada flag pode ser utilizada individualmente ou como um supercomando. 

**Ex:**<br>

`pyseed --counter 150 --url 192.168.0.1 --port 3778 --route register/client` -> Este comando faz com que o pyseed gere cento e cinquenta requisições e envie informações de username,email e password pela url `http://192.168.0.1:3778/register/client`.

**Assets:**<br>

**No caso de falha, o PySeed retorna:**<br>
<br>
!["PySeed Falhando"](/assets/FailurePySeed.png)<br>
<br>

**No caso de sucesso, o PySeed retorna:**<br>
<br>
!["PySeed rodando com sucesso"](/assets/SucessPySeed.png)<br>
<br>
