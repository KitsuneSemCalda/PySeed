#!/usr/bin/env python3

from urllib3._collections import HTTPHeaderDict
from os import system
from faker import Faker
import requests
import argparse

def generateNewUser(username, email, password, debug_mode):
    try:
        register = None
        if (debug_mode == False):
            register = HTTPHeaderDict()
            register.add('username', username)
            register.add('email', email)
            register.add('password', password)
            return register
        else:
            register = dict({'username': username, 'email':email, 'password':password})
            return register
    except Exception as e:
        print(f"\033[0;31;49m Tivemos um erro ao tentar gerar um dicionario http\033[m\t{e}\t\n")
pass

def sendRequest(url, port, route, debug_mode):
    try:
        FakeUser = Faker()
        username = FakeUser.user_name()
        email = FakeUser.email()
        password = FakeUser.password()
        if (debug_mode == False):
            newUrl = f"https://{url}:{port}/{route}"
            requests.post(newUrl,generateNewUser(username,email,password,debug_mode))
            time = requests.post(newUrl,generateNewUser(username,email,password,debug_mode)).elapsed.seconds
            print(f"\033[0;37;49mSend\033[0;36;49m {username}\033[0;37;49m to \033[0;32;49m{newUrl}\033[m\t\n\n")
            return time
        else:
            newUrl = f"https://{url}:{port}/{route}"
            dados = generateNewUser(username,email,password,debug_mode)
            print(f"\033[0;37;49m Send \033[0;36;49m {dados[username]} \n {dados[email]} \n {dados[password]} to \033[0;32;49m {newUrl} \033[m\t\n")
    except Exception as e:
        system("clear")
        print(f"\033[0;31;49m Tivemos um erro ao enviar tentar enviar uma requisição\033[m\t{e}\n")
    except KeyboardInterrupt:
        exit(0)
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    counter = parser.add_argument('--counter', type=int, default=5, help="changes the number of executions of request loop")
    url = parser.add_argument('--url', type=str, default="127.0.0.1", help="changes the url used to make request" )
    port = parser.add_argument('--port', type=str, default="3333", help="changes the port used to make request")
    route = parser.add_argument('--route', type=str, default="register", help="changes the route used")
    debug_mode = parser.add_argument('--debug', type=bool, default=False, help="uses debug mode")
    args = parser.parse_args()

    for i in range(args.counter):
        sendRequest(args.url, args.port, args.route, args.debug)
    pass
